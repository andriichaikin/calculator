package com.example.demo.instruction.impl;

import com.example.demo.instruction.Instruction;
import com.example.demo.exception.ComputationException;

import java.math.BigDecimal;

import static java.util.Objects.isNull;
import static org.springframework.util.StringUtils.isEmpty;

public class SubtractionInstruction implements Instruction {

    private static final String OPERATOR = "-";

    @Override
    public boolean isOperatorMatch(String operator) throws ComputationException {
        if (isEmpty(operator)) throw new ComputationException("Operator can not be null or empty");

        return OPERATOR.equals(operator.trim());
    }

    @Override
    public BigDecimal calculate(BigDecimal left, BigDecimal right) {
        if(isNull(left) || isNull(right))
            throw new ComputationException("Left [%s] or right [%s] operator is null", left, right);

        return left.subtract(right);
     }
}
