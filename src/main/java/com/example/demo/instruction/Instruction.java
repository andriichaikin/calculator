package com.example.demo.instruction;

import com.example.demo.exception.ComputationException;

import java.math.BigDecimal;

public interface Instruction {

    boolean isOperatorMatch(String operator) throws ComputationException;

    BigDecimal calculate(BigDecimal left, BigDecimal right);
}
