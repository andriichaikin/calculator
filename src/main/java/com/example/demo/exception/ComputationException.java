package com.example.demo.exception;

import static java.lang.String.format;

public class ComputationException extends CalculatorException {

    public ComputationException(String message) {
        super(message);
    }

    public ComputationException(String message, Object... args) {
        super(message, args);
    }

    public ComputationException(String message, Throwable cause) {
        super(message, cause);
    }
}
