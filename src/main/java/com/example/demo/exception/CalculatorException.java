package com.example.demo.exception;

import static java.lang.String.format;

public class CalculatorException extends RuntimeException {

    public CalculatorException(String message) {
        super(message);
    }

    public CalculatorException(String message, Object ...args) {
        super(format(message, args));
    }

    public CalculatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
