package com.example.demo.controller;

import com.example.demo.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class CalculatorController {

    @Autowired
    private CalculatorService calculatorService;

    @GetMapping(value = "/calculate")
    public ResponseEntity<BigDecimal> calculate(@RequestParam("left") BigDecimal left,
                                                @RequestParam("right") BigDecimal right,
                                                @RequestParam("operator") String operator) {

        var result = calculatorService.calculate(left, right, operator);

        return ResponseEntity.ok(result);
    }
}
