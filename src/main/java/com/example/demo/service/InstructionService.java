package com.example.demo.service;

import com.example.demo.instruction.Instruction;
import com.example.demo.exception.ComputationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

@Service
public class InstructionService {

    private static final String DELIMITER = ",";

    @Value("${instructions}")
    private String names;

    private List<Instruction> instructions = new LinkedList<>();

    public List<Instruction> getInstructions() {
        return instructions;
    }

    @PostConstruct
    private void init() {
        try {
            for (String name : names.split(DELIMITER)) {
                Instruction aClass = (Instruction) Class.
                        forName(name)
                        .getDeclaredConstructor()
                        .newInstance();

                instructions.add(aClass);
            }
        } catch (Exception ex) {
            throw new ComputationException("Can not create an instance of a class", ex);
        }
    }
}
