package com.example.demo.service;

import com.example.demo.instruction.Instruction;
import com.example.demo.exception.ComputationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static java.lang.String.format;
import static java.util.Objects.isNull;
import static org.apache.logging.log4j.util.Strings.isBlank;

@Service
public class CalculatorService {

    @Autowired
    private InstructionService instructionService;

    public BigDecimal calculate(BigDecimal left, BigDecimal right, String operator) {
        if (isNull(left) || isNull(right))
            throw new ComputationException(format("Left [%s] or right [%s] operand is null", left, right));
        if (isBlank(operator)) throw new ComputationException("Operator is null or empty");

        for (Instruction instruction : instructionService.getInstructions()) {
            if (instruction.isOperatorMatch(operator)) return instruction.calculate(left, right);
        }

        throw new ComputationException(format("Can not find instruction by operator [%s]", operator));
    }
}
