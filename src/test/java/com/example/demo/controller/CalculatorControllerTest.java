package com.example.demo.controller;

import com.example.demo.service.CalculatorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

import static java.lang.String.format;
import static java.net.URI.create;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.OK;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class CalculatorControllerTest {

    @LocalServerPort
    private int port;

    @MockBean
    private CalculatorService calculatorService;

    private HttpClient client = HttpClient.newHttpClient();

    @Test
    public void calculateTest() throws IOException, InterruptedException {
        var result = "98.8";
        var left = new BigDecimal("10");
        var right = new BigDecimal("88.8");

        String addOperator = "+";
        when(calculatorService.calculate(left, right, addOperator)).thenReturn(new BigDecimal(result));

        var addEncoded = URLEncoder.encode(addOperator, StandardCharsets.UTF_8);
        String uri = format("http://localhost:%s/calculate?left=%s&right=%s&operator=%s",
                port, left.toPlainString(), right.toPlainString(), addEncoded);
        var request = HttpRequest
                .newBuilder()
                .uri(create(uri))
                .build();

        var response = client.send(request, HttpResponse.BodyHandlers.ofString());

        assertThat(response.statusCode()).isEqualTo(OK.value());
        assertThat(response.body()).isEqualTo(result);

        verify(calculatorService, times(1)).calculate(left, right, addOperator);
    }
}
