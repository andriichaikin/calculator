package com.example.demo.service;

import com.example.demo.exception.ComputationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class CalculatorServiceTest {

    private static final String ADD = "+";
    private static final String SUBTRACT = "-";

    @Autowired
    private CalculatorService service;

    @Test
    public void calculateAddTest() {
        BigDecimal result = service.calculate(new BigDecimal("33.003"), new BigDecimal("11.5"), ADD);
        assertThat(result.equals(new BigDecimal("44.503"))).isTrue();

        BigDecimal res1 = service.calculate(new BigDecimal("100"), new BigDecimal("10"), ADD);
        assertThat(res1.equals(new BigDecimal("121"))).isFalse();
    }

    @Test
    public void calculateSubtractionTest() {
        BigDecimal result = service.calculate(new BigDecimal("33.5"), new BigDecimal("11.005"), SUBTRACT);
        assertThat(result.equals(new BigDecimal("22.495"))).isTrue();

        BigDecimal res1 = service.calculate(new BigDecimal(100), new BigDecimal(10), SUBTRACT);
        assertThat(res1.equals(new BigDecimal("999"))).isFalse();
    }

    @Test
    public void operatorNotFoundException() {
        assertThrows(ComputationException.class, () -> {
            service.calculate(new BigDecimal(100), new BigDecimal(10), "&&");
        });
    }
}
