package com.example.demo.instruction.impl;

import com.example.demo.instruction.Instruction;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class AdditionInstructionTest {

    private Instruction instruction = new AdditionInstruction();

    @Test
    public void addTrue() {
        var result = instruction.calculate(new BigDecimal(5.5), new BigDecimal(9));

        assertThat(result.equals(new BigDecimal(14.5))).isTrue();
    }

    @Test
    public void addFalse() {
        var result = instruction.calculate(new BigDecimal(5.5), new BigDecimal(9));

        assertThat(result.equals(new BigDecimal(15))).isFalse();
    }

    @Test
    public void isOperatorMatchTrue() {
        var result = instruction.isOperatorMatch("+");

        assertThat(result).isTrue();
    }

    @Test
    public void isOperatorMatchFalse() {
        var result = instruction.isOperatorMatch("/");

        assertThat(result).isFalse();
    }
}