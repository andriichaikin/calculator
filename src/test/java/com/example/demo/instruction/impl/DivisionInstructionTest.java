package com.example.demo.instruction.impl;

import com.example.demo.instruction.Instruction;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class DivisionInstructionTest {

    private Instruction instruction = new SubtractionInstruction();

    @Test
    public void subtractionTrue() {
        var result = instruction.calculate(new BigDecimal(5.5), new BigDecimal(9));
        System.out.println(result);
        assertThat(result.equals(new BigDecimal(-3.5))).isTrue();
    }

    @Test
    public void subtractionFalse() {
        var result = instruction.calculate(new BigDecimal(5.5), new BigDecimal(9));

        assertThat(result.equals(new BigDecimal(15))).isFalse();
    }

    @Test
    public void isOperatorMatchTrue() {
        var result = instruction.isOperatorMatch("-");

        assertThat(result).isTrue();
    }

    @Test
    public void isOperatorMatchFalse() {
        var result = instruction.isOperatorMatch("/");

        assertThat(result).isFalse();
    }
}
